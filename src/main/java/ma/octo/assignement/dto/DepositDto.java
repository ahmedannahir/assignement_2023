package ma.octo.assignement.dto;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter @Setter
public class DepositDto {
    private String nomPrenomEmetteur;
    private String rib;
    private BigDecimal montant;
    private String motif;
}
