package ma.octo.assignement.web;

import ma.octo.assignement.dto.DepositDto;
import ma.octo.assignement.entity.Compte;
import ma.octo.assignement.entity.util.EventType;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.service.AuditService;
import ma.octo.assignement.service.CompteService;
import ma.octo.assignement.service.DepositService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/deposits")
public class DepositController {

    private final DepositService depositService;
    private final CompteService compteService;
    private final AuditService auditService;

    @Autowired
    public DepositController(DepositService depositService, CompteService compteService, AuditService auditService) {
        this.depositService = depositService;
        this.compteService = compteService;
        this.auditService = auditService;
    }

    @PostMapping("create")
    public void create(@RequestBody DepositDto depositDTO) throws TransactionException, CompteNonExistantException {
        Compte compte = compteService.findByRib(depositDTO.getRib());

        depositService.checkRequirement(compte, depositDTO.getMontant(), depositDTO.getMotif(), depositDTO.getNomPrenomEmetteur());

        depositService.create(compte, depositDTO.getMontant(), depositDTO.getMotif(), depositDTO.getNomPrenomEmetteur());

        auditService.audit("Dépôt de la part de " + compte + "d'un montant de " + depositDTO.getMontant().toString(),
                EventType.DEPOSIT);
    }
}