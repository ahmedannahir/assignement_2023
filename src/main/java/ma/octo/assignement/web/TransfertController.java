package ma.octo.assignement.web;

import ma.octo.assignement.entity.Compte;
import ma.octo.assignement.entity.Transfert;
import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.entity.util.EventType;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.service.AuditService;
import ma.octo.assignement.service.CompteService;
import ma.octo.assignement.service.TransfertService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/transfers")
class TransfertController {

    private final CompteService compteService;
    private final AuditService auditService;
    private final TransfertService transfertService;

    @Autowired
    public TransfertController(CompteService compteService, AuditService auditService, TransfertService transfertService) {
        this.compteService = compteService;
        this.auditService = auditService;
        this.transfertService = transfertService;
    }

    @GetMapping("all")
    List<Transfert> loadAll() {
        return transfertService.findAll();
    }

    @PostMapping("/create")
    @ResponseStatus(HttpStatus.CREATED)
    public void createTransaction(@RequestBody TransferDto transferDto)
            throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException {

        Compte compteEmetteur = compteService.findByNrCompte(transferDto.getNrCompteEmetteur());
        Compte compteBeneficiaire = compteService.findByNrCompte(transferDto.getNrCompteBeneficiaire());

        transfertService.checkRequirements(compteEmetteur, compteBeneficiaire, transferDto.getMontant(),
                transferDto.getMotif());

        transfertService.createTransaction(compteEmetteur, compteBeneficiaire, transferDto.getMontant(),
                transferDto.getDate());

        auditService.audit("Transfer depuis " + transferDto.getNrCompteEmetteur() + " vers " + transferDto
                        .getNrCompteBeneficiaire() + " d'un montant de " + transferDto.getMontant()
                        .toString(), EventType.TRANSFER);
    }
}
