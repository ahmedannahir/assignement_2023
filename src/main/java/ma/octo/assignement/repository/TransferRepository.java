package ma.octo.assignement.repository;

import ma.octo.assignement.entity.Compte;
import ma.octo.assignement.entity.Transfert;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.Date;

@Repository
public interface TransferRepository extends JpaRepository<Transfert, Long> {
    @Query("SELECT SUM(t.montantTransfert) FROM Transfert t WHERE t.compteEmetteur =  :emetteur AND t.dateExecution >= :starting")
    BigDecimal getTransfersSumByAccountAndStartingDate(Compte emetteur, Date starting);
}
