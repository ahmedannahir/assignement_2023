package ma.octo.assignement.service;

import ma.octo.assignement.entity.Compte;
import ma.octo.assignement.repository.CompteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;

@Service
public class CompteService {

    private final CompteRepository compteRepository;

    @Autowired
    public CompteService(CompteRepository compteRepository) {
        this.compteRepository = compteRepository;
    }

    public List<Compte> findAll() {
        List<Compte> comptes = compteRepository.findAll();

        return CollectionUtils.isEmpty(comptes)? null : comptes;
    }

    public Compte findByNrCompte(String nrCompte) {
        return compteRepository.findByNrCompte(nrCompte);
    }

    public Compte findByRib(String rib) {
        return compteRepository.findByRib(rib);
    }
}
