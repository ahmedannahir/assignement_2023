package ma.octo.assignement.service;

import ma.octo.assignement.entity.Compte;
import ma.octo.assignement.entity.Transfert;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.TransferRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

@Service
public class TransfertService {

    public static final int MONTANT_MAXIMAL = 10000;
    public static final int MONTANT_MAXIMAL_JOURNALIER = 20000;
    Logger LOGGER = LoggerFactory.getLogger(TransfertService.class);
    private final CompteRepository compteRepository;
    private final TransferRepository transferRepository;

    @Autowired
    public TransfertService(CompteRepository compteRepository, TransferRepository transferRepository) {
        this.compteRepository = compteRepository;
        this.transferRepository = transferRepository;
    }

    public List<Transfert> findAll() {
        var transfers = transferRepository.findAll();

        return CollectionUtils.isEmpty(transfers) ? null : transfers;
    }

    public void checkRequirements(Compte compteEmetteur, Compte compteBeneficiaire, BigDecimal montant, String motif)
            throws CompteNonExistantException, TransactionException, SoldeDisponibleInsuffisantException {

        if (compteEmetteur == null || compteBeneficiaire == null) {
            LOGGER.error("Compte non existant");
            throw new CompteNonExistantException("Compte non existant");
        }

        if (montant == null || montant.compareTo(BigDecimal.ZERO) == 0) {
            LOGGER.error("Montant vide");
            throw new TransactionException("Montant vide");
        }

        if (montant.compareTo(BigDecimal.TEN) < 0) {
            LOGGER.error("Montant minimal de transfer non atteint");
            throw new TransactionException("Montant minimal de transfer non atteint");
        } else if (montant.compareTo(BigDecimal.valueOf(MONTANT_MAXIMAL)) > 0) {
            LOGGER.error("Montant maximal de transfer dépassé");
            throw new TransactionException("Montant maximal de transfer dépassé");
        }

        if (motif.isEmpty() || motif.isBlank()) {
            LOGGER.error("Motif vide");
            throw new TransactionException("Motif vide");
        }

        if (compteEmetteur.getSolde().compareTo(montant) < 0) {
            LOGGER.error("Solde insuffisant pour l'utilisateur");
            throw new SoldeDisponibleInsuffisantException("Solde insuffisant pour l'utilisateur");
        }

        // Get today at midnight date
        Calendar today = new GregorianCalendar();
        today.set(Calendar.HOUR_OF_DAY, 0);
        today.set(Calendar.MINUTE, 0);
        today.set(Calendar.SECOND, 0);
        today.set(Calendar.MILLISECOND, 0);

        BigDecimal dailyTransfersSum = this.transferRepository
                .getTransfersSumByAccountAndStartingDate(compteEmetteur, today.getTime());

        dailyTransfersSum = dailyTransfersSum == null ? BigDecimal.ZERO : dailyTransfersSum;

        if (dailyTransfersSum.add(montant).compareTo(BigDecimal.valueOf(MONTANT_MAXIMAL_JOURNALIER)) > 0) {
            LOGGER.error("Montant maximal journalier depassé");
            throw new TransactionException("Montant maximal journalier depassé");
        }
    }

    public void createTransaction(Compte compteEmetteur, Compte compteBeneficiaire, BigDecimal montant,
                                  Date transfertDate){

        compteEmetteur.setSolde(compteEmetteur.getSolde().subtract(montant));
        compteRepository.save(compteEmetteur);

        compteBeneficiaire.setSolde(compteBeneficiaire.getSolde().add(montant));
        compteRepository.save(compteBeneficiaire);

        Transfert transfer = new Transfert();
        transfer.setDateExecution(transfertDate);
        transfer.setCompteBeneficiaire(compteBeneficiaire);
        transfer.setCompteEmetteur(compteEmetteur);
        transfer.setMontantTransfert(montant);

        transferRepository.save(transfer);
    }
}
