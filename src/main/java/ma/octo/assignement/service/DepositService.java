package ma.octo.assignement.service;

import ma.octo.assignement.entity.Compte;
import ma.octo.assignement.entity.MoneyDeposit;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.MoneyDepositRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Date;

@Service
public class DepositService {
    public static final int MONTANT_MAXIMAL = 10000;
    Logger LOGGER = LoggerFactory.getLogger(DepositService.class);
    private final MoneyDepositRepository moneyDepositRepository;
    private final CompteRepository compteRepository;

    @Autowired
    public DepositService(MoneyDepositRepository moneyDepositRepository, CompteRepository compteRepository) {
        this.moneyDepositRepository = moneyDepositRepository;
        this.compteRepository = compteRepository;
    }


    public void checkRequirement(Compte compte, BigDecimal montant, String motif, String nomPrenomEmetteur)
            throws CompteNonExistantException, TransactionException {
        if (compte == null) {
            LOGGER.error("Compte non existant");
            throw new CompteNonExistantException("Compte non existant");
        }

        if (montant == null || montant.compareTo(BigDecimal.ZERO) == 0) {
            LOGGER.error("Montant vide");
            throw new TransactionException("Montant vide");
        }

        if (montant.compareTo(BigDecimal.valueOf(MONTANT_MAXIMAL)) > 0) {
            LOGGER.error("Montant maximal de depot dépassé");
            throw new TransactionException("Montant maximal de depot dépassé");
        }

        if (motif.isEmpty() || motif.isBlank()) {
            LOGGER.error("Motif vide");
            throw new TransactionException("Motif vide");
        }

        if (nomPrenomEmetteur.isEmpty() || nomPrenomEmetteur.isBlank()) {
            LOGGER.error("Nom de l'emetteur vide");
            throw new TransactionException("Nom de l'emetteur vide");
        }
    }

    public void create(Compte compte, BigDecimal montant, String motif, String nomPrenomEmetteur) {
        compte.setSolde(compte.getSolde().add(montant));
        compteRepository.save(compte);

        MoneyDeposit moneyDeposit = new MoneyDeposit();
        moneyDeposit.setMontant(montant);
        moneyDeposit.setCompteBeneficiaire(compte);
        moneyDeposit.setDateExecution(new Date());
        moneyDeposit.setMotifDeposit(motif);
        moneyDeposit.setNomPrenomEmetteur(nomPrenomEmetteur);
        moneyDepositRepository.save(moneyDeposit);

    }
}
