package ma.octo.assignement.service;

import ma.octo.assignement.entity.Utilisateur;
import ma.octo.assignement.repository.UtilisateurRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;

@Service
public class UtilisateurService {

    private final UtilisateurRepository utilisateurRepository;

    @Autowired
    public UtilisateurService(UtilisateurRepository utilisateurRepository) {
        this.utilisateurRepository = utilisateurRepository;
    }

    public List<Utilisateur> findAll() {
        List<Utilisateur> utilisateurs = utilisateurRepository.findAll();

        return CollectionUtils.isEmpty(utilisateurs)? null : utilisateurs;
    }
}
