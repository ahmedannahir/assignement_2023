package ma.octo.assignement.service;

import ma.octo.assignement.entity.AuditTransfer;
import ma.octo.assignement.entity.util.EventType;
import ma.octo.assignement.repository.AuditTransferRepository;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class AuditService {

    Logger LOGGER = LoggerFactory.getLogger(AuditService.class);

    private final AuditTransferRepository auditTransferRepository;

    @Autowired
    public AuditService(AuditTransferRepository auditTransferRepository) {
        this.auditTransferRepository = auditTransferRepository;
    }

    public void audit(String message, EventType type) {

        LOGGER.info("Audit de l'événement {}", type);

        AuditTransfer audit = new AuditTransfer();
        audit.setEventType(type);
        audit.setMessage(message);
        auditTransferRepository.save(audit);
    }
}
