package ma.octo.assignement.service;

import ma.octo.assignement.entity.Compte;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.TransferRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.GregorianCalendar;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class TransfertServiceTest {

    private CompteRepository compteRepository;

    private TransferRepository transferRepository;

    private TransfertService transfertService;

    private Compte emetteur;
    private Compte beneficiaire;
    private Calendar today;

    @BeforeEach
    public void init() {

        compteRepository = mock(CompteRepository.class);
        transferRepository = mock(TransferRepository.class);
        transfertService = new TransfertService(compteRepository, transferRepository);

        emetteur = new Compte();
        emetteur.setNrCompte("010000A000001000");
        emetteur.setRib("RIB1");
        emetteur.setSolde(BigDecimal.valueOf(200000L));
        emetteur.setUtilisateur(null);

        beneficiaire = new Compte();
        beneficiaire.setNrCompte("010000B025001000");
        beneficiaire.setRib("RIB2");
        beneficiaire.setSolde(BigDecimal.valueOf(140000L));
        beneficiaire.setUtilisateur(null);

        today = new GregorianCalendar();
        today.set(Calendar.HOUR_OF_DAY, 0);
        today.set(Calendar.MINUTE, 0);
        today.set(Calendar.SECOND, 0);
        today.set(Calendar.MILLISECOND, 0);
    }

    @Test
    public void should_throw_exception_when_daily_cap_surpassed() {

        when(transferRepository.getTransfersSumByAccountAndStartingDate(emetteur, today.getTime()))
                .thenReturn(BigDecimal.valueOf(15000L));

        TransactionException exception = assertThrows(TransactionException.class, () -> {
            this.transfertService.checkRequirements(emetteur, beneficiaire, BigDecimal.valueOf(6000L), "Virement");
        });

        assertEquals("Montant maximal journalier depassé", exception.getMessage());
    }

    @Test
    public void should_pass_when_daily_cap_respected() {

        when(transferRepository.getTransfersSumByAccountAndStartingDate(emetteur, today.getTime()))
                .thenReturn(BigDecimal.valueOf(15000L));

        assertDoesNotThrow(() -> {
            this.transfertService.checkRequirements(emetteur, beneficiaire, BigDecimal.valueOf(4000L), "Virement");
        });
    }
}
